title:      "Reading 08: Patent Trolls, Open Source"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for **Tuesday, March 15** are:

  ### Intellectual Property

  1. [WIPO - What is IP?](http://www.wipo.int/edocs/pubdocs/en/intproperty/450/wipo_pub_450.pdf)

  2. [US Constitution - Copright Clause](https://en.wikipedia.org/wiki/Copyright_Clause)

  3. [Thomas Jefferson to Isaac McPherson](http://press-pubs.uchicago.edu/founders/documents/a1_8_8s12.html)

  ### Patent Trolls

  1. [The History of Software Patents: From Benson, Flook, and Diehr to Bilski and Mayo v. Prometheus](http://www.bitlaw.com/software-patent/history.html)

  2. [When Patents Attack!](http://www.thisamericanlife.org/radio-archives/episode/441/transcript)

      - [Jury: Apple must pay $626 million to patent troll VirnetX](http://arstechnica.com/tech-policy/2016/02/jury-apple-must-pay-626-million-to-patent-troll-virnetx/)

      - [IBM sues Groupon over 1990s patents related to Prodigy](http://arstechnica.com/tech-policy/2016/03/ibm-sues-groupon-saying-it-infringes-patents-related-to-1990s-prodigy-service/)

      - [Patent troll lawsuits head toward all-time high](http://arstechnica.com/tech-policy/2015/07/patent-troll-lawsuits-head-towards-all-time-high/)

      - ["Patent troll" with a big verdict against Cisco notches a Supreme Court win](http://arstechnica.com/tech-policy/2015/05/patent-troll-with-a-big-verdict-against-cisco-notches-a-supreme-court-win/)

      - [East Texas judge throws out 168 patent cases in one fell swoop](http://arstechnica.com/tech-policy/2015/10/east-texas-judge-throws-out-168-patent-cases-in-one-fell-swoop/)

  3. [The Case Against Patents](http://www.npr.org/sections/money/2014/07/09/329895088/episode-551-the-case-against-patents)

  4. [Analysis: Microsoft's software patent flip-flop](http://arstechnica.com/business/2007/03/analysis-microsofts-software-patent-flip-flop/)

  5. [All Our Patent Are Belong To You](https://www.teslamotors.com/blog/all-our-patent-are-belong-you)

  6. [The Supreme Court doesn't understand software, and that's a problem](http://www.vox.com/2014/6/20/5824426/the-supreme-court-doesnt-understand-software-and-thats-a-problem)

  ### Open Source

  1. [Open Source World](http://www.npr.org/programs/ted-radio-hour/449179937/open-source-world)

      - [The new open-source economics](https://www.ted.com/talks/yochai_benkler_on_the_new_open_source_economics?language=en)

  2. [Why Open Source misses the point of Free Software](http://www.gnu.org/philosophy/open-source-misses-the-point.en.html)

      - [What is free software?](http://www.gnu.org/philosophy/free-sw.html)

      - [Why you should use a BSD style license for your Open Source Project](https://www.freebsd.org/doc/en/articles/bsdl-gpl/index.html)

  3. [Freeing the Source - The Story of Mozilla](http://www.oreilly.com/openbook/opensources/book/netrev.html)

  4. [A Generation Lost in the Bazaar](http://queue.acm.org/detail.cfm?id=2349257)

  5. [Corporations and OSS Do Not Mix](http://www.coglib.com/~icordasc/blog/2015/11/corporations-and-oss-do-not-mix.html)

  6. [Leveraging American Ingenuity through Reusable and Open Source Software](https://www.whitehouse.gov/blog/2016/03/09/leveraging-american-ingenuity-through-reusable-and-open-source-software)

  ## Questions

  Please write a response to one of the following questions:

  1. From the readings, what exactly are patents?  What are the ethical, moral,
  economic, or social reasons for granting patents?

      In your opinion, should patents be granted at all?  Are they really
      necessary or beneficial for society?  Do the promote innovation or do
      they hinder it?  Explain.

      Additionally, should patents on software be granted or should patents be
      restricted to physical or more tangible artifacts? Explain.

      Finally, is the existence of [patent trolls] evidence that the patent
      system is working or that the system is broken?  Explain.

  2. From the readings, what exactly is copyright?  What are the ethical,
  moral, economic, or social reasons for granting copyrights?

      When should an open source license be preferred over a proprietary
      license?  Is open source software inherently better?  If so, how do you
      explain things like [HeartBleed] and [ShellShock]?

      Is the distinction between [free software] and [open source] meaningful?
      Between the [GPL] and [BSD] license, which one do you prefer?  Which one
      is more **free**?

      Should governments and other public organizations be encourage to adopt
      and support open source software?  What responsibilities do organizations
      and companies that utilize open source have to the open source projects
      and community at large?

  [patent trolls]:  https://en.wikipedia.org/wiki/Patent_troll
  [GPL]:            https://en.wikipedia.org/wiki/GNU_General_Public_License
  [BSD]:            https://en.wikipedia.org/wiki/BSD_licenses
  [HeartBleed]:     http://heartbleed.com/
  [ShellShock]:     https://en.wikipedia.org/wiki/Shellshock_(software_bug)

  ## Feedback

  If you have any questions, comments, or concerns regarding the course, please
  provide your feedback at the end of your response.
